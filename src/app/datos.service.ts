import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable'
@Injectable({
  providedIn: 'root'
})
export class DatosService {
  datos: Observable<any>;
  dato:Observable<any>;
  api='http://168.232.165.184/prueba'


  constructor(private http: HttpClient) { }

  obtenerArray(){
    const path= `${this.api}/array`;
    return this.http.get(path);
  }

  obtenerDatos(){
    const path= `${this.api}/dict`;
    return this.http.get(path);
  }
}
